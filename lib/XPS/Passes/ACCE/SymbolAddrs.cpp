
#include <cmath>
#include <set>
#include <fstream>
#include "llvm/Support/raw_ostream.h"
#include "llvm/IR/InstrTypes.h"
#include "llvm/IR/Instruction.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/DebugInfo.h"
#include <llvm/IR/CallSite.h>
#include <llvm/IR/Constants.h>
#include <llvm/IR/Dominators.h>
#include <llvm/Analysis/LoopInfo.h>
#include "llvm/IR/Value.h"
#include "llvm/IR/PassManager.h"
#include "llvm/Analysis/LoopPass.h"
#include "llvm/Analysis/LoopAnalysisManager.h"
#include "llvm/IR/CFG.h"
#include "Utils/XPSTypeCache.h"
#include "Utils/PassCommons.h"

using namespace llvm;

namespace xps {

typedef std::string string;

class SymbolAddrsPass : public ModulePass {
  int _ic_id;
  Module* _m;
  XPSTypeCache _tc;
public:
  static char ID;
public:
  SymbolAddrsPass(): ModulePass(ID) {
    _ic_id = 0;
  }

  ~SymbolAddrsPass() {

  }

  GlobalVariable* addGlobalStr(Twine name, Twine value) {
    Constant *c = ConstantDataArray::getString(_m->getContext(),
                                               value.str());
    auto *gv = new GlobalVariable(*_m, c->getType(), true,
                                  GlobalValue::ExternalLinkage,
                                  c, name);
    return gv;
  }

  void addFunctionStrDecl(Function& F) {
    addGlobalStr("xps."+F.getName()+".in", F.getName());
  }

  Instruction* getFirstInst(Function& F) {
    for (auto& B: F) {
      for (auto& I: B) {
        return &I;
      }
    }
  }

  void instrumentFunctionEntry(Function& F) {
    if (F.isDeclaration() || F.isIntrinsic()) {
      return;
    }
    auto gv = addGlobalStr("xps."+F.getName()+".in",
                           F.getName());

    Instruction* FI = getFirstInst(F);
    auto bc = new BitCastInst(gv, _tc.Int8PtrTy, "", FI);
    auto bc1 = new BitCastInst(&F, _tc.Int8PtrTy, "", FI);
    std::vector<Value*> args { bc, bc1 };
    Function* callee = _m->getFunction("xps_log_func");
    CallInst *new_inst = CallInst::Create(callee, args, "", FI);
  }

  void instrumentIndiAndEntry() {
    for (auto& F: *_m) {
      instrumentFunctionEntry(F);
      for (auto& B: F) {
        for (auto& I: B) {
          if (CallSite CS = CallSite(&I)) {
            if (CS.isIndirectCall()) {
              doCall(CS);
            }
          }
        }
      }
    }
  }

  void doCall(CallSite CS) {
    Instruction* I = CS.getInstruction();
    auto id = ConstantInt::get(_tc.Int32Ty, _ic_id++);
    auto bc = new BitCastInst(CS.getCalledValue(),
                               _tc.Int8PtrTy, "", I);
    std::vector<Value*> args { id, bc };
    Function* callee = _m->getFunction("xps_log_ic");
    CallInst *new_inst = CallInst::Create(callee, args, "", I);
  }

  void insertLoggerDecl() {
    std::vector<Type*> arg_types{ _tc.Int8PtrTy, _tc.Int8PtrTy };
    // FT: void (*f) (void* name, void* address)
    FunctionType *FT = FunctionType::get(_tc.VoidTy, arg_types, false);
    Function::Create(FT, Function::ExternalLinkage, "xps_log_func", _m);

    std::vector<Type*> arg_types1{ _tc.Int32Ty, _tc.Int8PtrTy};
    // FT: void (*f) (int id, void* address)
    FunctionType *FT1 = FunctionType::get(_tc.VoidTy, arg_types1, false);
    Function::Create(FT1, Function::ExternalLinkage, "xps_log_ic", _m);
  }
  
  bool runOnModule(Module& M) override {
    _m = &M;
    _tc.initialize(M.getContext());

    insertLoggerDecl();
    instrumentIndiAndEntry();
    return true;
  }
};

char SymbolAddrsPass::ID = 0;

static RegisterPass<SymbolAddrsPass>
    SymbolAddrsPassInfo("symbol-addrs", "");
}
