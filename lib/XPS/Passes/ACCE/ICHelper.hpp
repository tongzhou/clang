//
// Created by tzhou on 2/18/18.
//

#ifndef LLVM_LOADIC_HPP
#define LLVM_LOADIC_HPP

#include <string>
#include <fstream>
#include <cassert>
#include <Utils/PassCommons.h>

typedef std::string string;

using namespace llvm;

namespace xps {
class ICHelper {
  Module *_m;
  int _ic_id;
  std::map<int, Instruction*> _ics;
  std::map<Function*, std::set<Instruction*>> _callers;
  std::map<Instruction*, std::set<Function *>> _targets;
public:
  ICHelper() {

  }

  void cnstructICIDMap() {
    int ic_id = 0;
    for (auto& F: *_m) {
      for (auto& B: F) {
        for (auto& I: B) {
          if (CallSite CS = CallSite(&I)) {
            if (CS.isIndirectCall()) {
              _ics[ic_id] = CS.getInstruction();
              ic_id++;
            }
          }
        }
      }
    }
  }

  void loadICProfile(string& file, Module* m) {
    _m = m;
    cnstructICIDMap();

    std::ifstream ifs(file);
    if (!ifs.good()) {
      errs() << "open file " << file << " failed\n";
      return;
    }
    
    string line;
    while (std::getline(ifs, line)) {
      int sp = line.find(' ');
      int f1 = std::stoi(line.substr(0, sp));
      string f2 = line.substr(sp + 1);
      Instruction* IC = _ics.at(f1);
      if (_targets.find(IC) == _targets.end()) {
        _targets[IC] = std::set<Function *>();
      }
      auto F = _m->getFunction(f2);
      assert(F);
      _targets[IC].insert(F);

      if (_callers.find(F) == _callers.end()) {
        _callers[F] = std::set<Instruction*>();
      }
      _callers[F].insert(IC);
    }
  }

  void getAllCallees(std::set<Function *> &callees,
                     CallSite CS) {
    if (!CS.isIndirectCall()) {
      callees.insert(get_callee(CS));
    }
    else {
      auto I = CS.getInstruction();
      if (_targets.find(I) != _targets.end()) {
        auto& set = _targets[I];
        callees.insert(set.begin(), set.end());
      }
    }
  }

  void getICCallers(Function* F, std::set<Instruction*>& callers) {
    if (_callers.find(F) == _callers.end()) {
      return;
    }
    else {
      callers = _callers[F];
    }
  }

  Function* getHighestCallee(std::map<Function*, int>& levels,
                            CallSite CS) {
    if (!CS.isIndirectCall()) {
      return get_callee(CS);
    }
    else {
      auto& set = _targets[CS.getInstruction()];
      int highest = 0;
      Function* final = NULL;
      for (auto& callee: set) {
        if (levels.at(callee) > highest) {
          highest = levels.at(callee);
          final = callee;
        }
      }
      return final;
    }
  }
};
}
#endif //LLVM_LOADIC_HPP
