//
// Created by tzhou on 2/5/18.
//


#include <cmath>
#include <set>
#include <fstream>
#include "llvm/Support/raw_ostream.h"
#include "llvm/IR/InstrTypes.h"
#include "llvm/IR/Instruction.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/DebugInfo.h"
#include <llvm/IR/CallSite.h>
#include <llvm/IR/Constants.h>
#include <llvm/IR/Dominators.h>
#include <llvm/Analysis/LoopInfo.h>
#include "llvm/IR/Value.h"
#include "llvm/IR/PassManager.h"
#include "llvm/Analysis/LoopPass.h"
#include "llvm/Analysis/LoopAnalysisManager.h"
#include "llvm/IR/CFG.h"
#include "Utils/XPSTypeCache.h"

using namespace llvm;

namespace xps {

typedef std::string string;

class IndiAnalysisPass : public ModulePass {
  int _id;
  Module* _m;
  XPSTypeCache _tc;
  std::ofstream _ofs;
  std::vector<size_t> _counts;
public:
  static char ID;
public:
  IndiAnalysisPass(): ModulePass(ID) {
    _id = 0;
    _counts.assign(2, 0);
  }

  ~IndiAnalysisPass() {

  }

  void doCallSite(CallSite CS) {
    if (CS.isIndirectCall()) {
      _counts[1]++;
    }
    _counts[0]++;
  }

  void doFunction(Function& F) {
    if (F.isDeclaration() || F.isIntrinsic()) {
      return;
    }

    for (auto& B: F) {
      for (auto& I: B) {
        if (CallSite CS = CallSite(&I)) {
          doCallSite(CS);
        }
      }
    }
  }

  void report() {
    outs() << "site: " << _counts[0] << "\n"
           << "indi: " << _counts[1] << " ("
           << float(_counts[1])/_counts[0] << ")"
           << "\n"
      ;
  }

  bool runOnModule(Module& M) override {
    _m = &M;
    _tc.initialize(M.getContext());

    for (auto& F: M) {
      doFunction(F);
    }

    report();
    return false;
  }
};

char IndiAnalysisPass::ID = 0;

static RegisterPass<IndiAnalysisPass>
    IndiAnalysisPassInfo("indi", "");

}
