//
// Created by tzhou on 12/23/17.
//

#ifndef XPS_PCCE_H
#define XPS_PCCE_H

#include <cmath>
#include <set>
#include <fstream>
#include <llvm/Support/raw_ostream.h>
#include <llvm/Support/FileSystem.h>
#include <llvm/Transforms/IPO/PassManagerBuilder.h>
#include <llvm/IR/LegacyPassManager.h>
#include <clang/XPS/Flags.h>
#include <Utils/XPSTypeCache.h>
#include <Utils/PassCommons.h>
#include <fcntl.h>
#include "BackEdgeHelper.hpp"
#include "TopoSort.hpp"
#include <Utils/CallGraphBase.h>
#include "../HCCE/SCC.hpp"

using namespace llvm;

namespace xps {

typedef std::string string;

static cl::opt<string> QCaller("caller");
static cl::opt<string> QCallee("callee");
static cl::opt<bool> DoBE("back-edge", cl::init(true));
static cl::opt<bool> Baseline("baseline");
static cl::opt<bool> PrintPaths("print-paths");
static cl::opt<bool> PrintStats("print-stats");
static cl::opt<bool> DynCallLogger("dyn-logger");
// SCC mode
// 1: Use PCCE just for SCC internal edges
// 2: Use PCCE just for SCCs that are not optimized by HCCE
static cl::opt<int> SCCMode("scc-mode", cl::init(0));

// Option: nlevel now means the layers of contexts
// nlevel=0: no context;
// nlevel=1: 1 layer of context; etc.

class PCCEPass: public ModulePass, public XPSTypeCache {
public:
  int _time = 0;
  int _max_level = 0;
  int _be_counter = 0;
  Type* _ctx_id_ty;
  bool _verbose = true;
  bool _use_stride = true;
  bool _record_cv = false;
  bool _opt_zero_inc = true;

  Module* _m = nullptr;
  SCCHelper* _scc;
  std::vector<size_t> _ids;
  std::vector<size_t> _counts;
  std::vector<Function*> _topo;
  std::map<Function*, size_t > _num_cc;
  std::map<Function*, int> _func_levels;
  std::set<Instruction*> _back_edges;
  std::set<Function*> _root_nodes;
  std::map<Function*, int> _callers;
  std::set<string> _query_funcs;
  std::ofstream _log;

public:
  static char ID;

  PCCEPass(): ModulePass(ID) {
    _counts.assign(2, 0);

    if (Baseline) {
      _opt_zero_inc = false;
    }
  }

  ~PCCEPass() {

  }

  void initQueryFunctions() {
    _query_funcs.insert("malloc");
    _query_funcs.insert("calloc");
    _query_funcs.insert("realloc");
    _query_funcs.insert("free");
  }

  bool isQueryFunction(Function* F) {
    return _query_funcs.find(F->getName().str())
        != _query_funcs.end();
  }

  bool isBackEdge(Instruction* I) {
    return _back_edges.find(I) != _back_edges.end();
  }

  void annotateRecursive() {
    auto main = _m->getFunction("main");
    assert(main);
    _root_nodes.insert(main);
    for (auto I: _back_edges) {
      _root_nodes.insert(get_callee(CallSite(I)));
    }

    for (auto& F: *_m) {
      _num_cc[&F] = 0;
    }

    //_num_cc[main] = 0;

    for (int i = _topo.size()-1; i >= 0; --i) {
      auto f = _topo[i];
      //outs() << f->getName() << " ";

      if (_root_nodes.find(f) != _root_nodes.end()) {
        _num_cc[f]++;
      }

      for (auto I: get_callers(f)) {
        auto p = I->getFunction();
        _num_cc[f] += _num_cc[p];
      }
      //outs() << _num_cc[f] << "\n";
    }
  }

  void annotate() {
    for (auto& F: *_m) {
      _num_cc[&F] = 0;
    }

    auto main = _m->getFunction("main");
    assert(main);
    _num_cc[main] = 1;

    for (int i = _topo.size()-1; i >= 0; --i) {
      auto f = _topo[i];
      //outs() << f->getName() << " ";
      for (auto I: get_callers(f)) {
        auto p = I->getFunction();
        _num_cc[f] += _num_cc[p];
      }
      //outs() << _num_cc[f] << "\n";
    }
  }

  void instrumentCallSites() {
    for (auto& F: *_m) {
      if (_num_cc[&F] == 0) {
        continue;
      }

      if (!has_definition(&F)) {
        continue;
      }
      int s = 0;
      if (_root_nodes.find(&F) != _root_nodes.end()) {
        s = 1;
      }

      for (auto I: get_callers(&F)) {
        auto p = I->getFunction();
        if (isBackEdge(I)) {
          doBackEdge(CallSite(I));
          continue;
        }

        doCallSite(CallSite(I), &F, s);
        s += _num_cc[p];
      }
    }
  }

  void doBackEdge(CallSite CS) {
    if (!DoBE) {
      return;
    }

    auto I = CS.getInstruction();
    auto caller = I->getFunction();
    auto callee = get_callee(CS);

    if (SCCMode > 1) {
      int scc = _scc->getSCCID(caller);
      if (_scc->isOptimizableSCC(scc)) {
        return;
      }
    }
//    int l1 = _num_cc.at(caller);
//    int l2 = _num_cc.at(callee);

    std::vector<Value*> args{ ConstantInt::get(Int64Ty, _be_counter++) };
    Function* xps_push_ctx = _m->getFunction("pcce_push_ctx");
    CallInst::Create(xps_push_ctx, args, "", I);

    std::vector<Value*> args1;
    Function* xps_pop_ctx = _m->getFunction("pcce_pop_ctx");
    for (auto s: get_succ_insts(I)) {
      CallInst::Create(xps_pop_ctx, args1, "", s);
    }
  }

  void doCallSite(CallSite CS, Function* callee, int s) {
    if (!has_definition(callee)) {
      return;
    }

    auto I = CS.getInstruction();

    if (SCCMode) {
      if (!_scc->isInternalEdge(CS)) {
        return;
      }
    }

    if (CS.isIndirectCall()) {
      outs() << I->getFunction()->getName() << " -> "
             << callee->getName() << "\n";
    }
    assert(!isBackEdge(I));

    if (s == 0 && _opt_zero_inc) {
      return;
    }

    Function* caller = I->getFunction();
    insertIncrement(I, s);


    if (auto i = dyn_cast<InvokeInst>(I)) {
      insertDecrement(i->getNormalDest()->getFirstNonPHI(), s);
      insertDecrement(i->getLandingPadInst()->getNextNode(), s);
    }
    else {
      insertDecrement(I->getNextNode(), s);
    }

    /* number of instrumented sites */
    _counts[0]++;
    _counts[1]++;
  }

  void insertIncrement(Instruction* I, int value) {
    auto ctx_id = _m->getGlobalVariable("pcce_id");
    auto load = new LoadInst(ctx_id, "", I);
    auto v = ConstantInt::get(_ctx_id_ty, value);
    auto add = BinaryOperator::Create(Instruction::Add,
                                          load, v, "", I);
    auto store = new StoreInst(add, ctx_id, I);
  }

  void insertDecrement(Instruction* I, int value) {
    auto ctx_id = _m->getGlobalVariable("pcce_id");
    auto load = new LoadInst(ctx_id, "", I);
    auto v = ConstantInt::get(_ctx_id_ty, value);
    auto sub = BinaryOperator::Create(Instruction::Sub,
                                      load, v, "", I);
    auto store = new StoreInst(sub, ctx_id, I);
  }

  void appendNewGlobal() {
    auto gv = new GlobalVariable(*_m, _ctx_id_ty, false,
                                 GlobalValue::ExternalLinkage,
                                 nullptr, "pcce_id", nullptr);
    insertPushAndPopDecl();
  }

  void insertPushAndPopDecl() {
    std::vector<Type*> arg_types{ Int64Ty };
    FunctionType *FT = FunctionType::get(VoidTy, arg_types, false);
    Function::Create(FT, Function::ExternalLinkage, "pcce_push_ctx", _m);

    std::vector<Type*> arg_types1{ };
    FunctionType *FT1 = FunctionType::get(VoidTy, arg_types1, false);
    Function::Create(FT1, Function::ExternalLinkage, "pcce_pop_ctx", _m);

  }

  void printQueryFuncStats() {
    for (auto name: _query_funcs) {
      Function* f = _m->getFunction(name);
      if (!f) {
        continue;
      }
      outs() << "[" << name << "]\n"
             << "  num_cc: " << _num_cc[f] << '\n'
          ;
    }
  }

  void printIDStats() {
    outs() << "[ids]\n";
    for (int i = 0; i < _ids.size(); ++i) {
      outs() << "  " << i << ": " << _ids[i] << '\n';
    }
  }

  void printStats() {
    printIDStats();
    errs() << "max: " << _max_level << "\n";
    errs() << "sites: " << _counts[0] << "\n";

    printQueryFuncStats();
    printStatsToFile();
  }

  void printStatsToFile() {
    FILE* f = fopen("cr-funcs.txt", "w");
    raw_fd_ostream OS(fileno(f), true);
    for (auto it: _func_levels) {
      Function* f = it.first;
      int l = it.second;
      if (l == -1) {
        continue;
      }
      OS << f->getName() << " " << l << "\n";
    }
    OS.close();
  }

  bool runOnModule(Module& M) override {
    XPSTypeCache::initialize(M.getContext());
    _m = &M;
    if (SCCMode) {
      _scc = new SCCHelper(_m);
      _scc->markSCC();
      _scc->markSimpleSCCs();
    }

    _ctx_id_ty = Int64Ty;
    TopoSort ts;
    ts.sort(_topo, M);

    initQueryFunctions();
    BackEdgeHelper beh;
    beh.markBackEdge(_back_edges, M);
    beh.report();
    
    appendNewGlobal();
    annotateRecursive();
    instrumentCallSites();

    if (PrintStats) {
      printStats();
    }

    return true;
  }
};

char PCCEPass::ID = 0;


static RegisterPass<PCCEPass> PCCEPassInfo("pcce", "PCCE Pass",
                                               false /* Only looks at CFG */,
                                               false /* Analysis Pass */);

}

#endif

