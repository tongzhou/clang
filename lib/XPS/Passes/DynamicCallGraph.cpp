//
// Created by tzhou on 1/21/18.
//

#include <cmath>
#include <set>
#include <fstream>
#include "llvm/Support/raw_ostream.h"
#include "llvm/IR/InstrTypes.h"
#include "llvm/IR/Instruction.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/DebugInfo.h"
#include <llvm/IR/CallSite.h>
#include <llvm/IR/Constants.h>
#include <llvm/IR/Dominators.h>
#include <llvm/Analysis/LoopInfo.h>
#include "llvm/IR/Value.h"
#include "llvm/IR/PassManager.h"
#include "llvm/Analysis/LoopPass.h"
#include "llvm/Analysis/LoopAnalysisManager.h"
#include "llvm/IR/CFG.h"

using namespace llvm;

namespace xps {

typedef std::string string;

class DynamicCallGraphPass : public ModulePass {
  Module* _m;
public:
  static char ID;
public:
  DynamicCallGraphPass(): ModulePass(ID) {}

  void addGlobalStr(Twine name, Twine value) {
    Constant *c = ConstantDataArray::getString(_m->getContext(), value.str());
    auto *gv = new GlobalVariable(*_m, c->getType(), true,
                                  GlobalValue::ExternalLinkage, c, name);
  }

  void addFunctionStrDecl(Function& F) {
    addGlobalStr("xps."+F.getName()+".in", "<xps>: "+F.getName()+"\n");
    // addGlobalStr("xps."+F.getName()+".out", "<xps>: "+F.getName()+"\n");
  }

  Instruction* getFirstInst(Function& F) {
    for (auto& B: F) {
      for (auto& I: B) {
        return &I;
      }
    }
  }

  Type* getInt8PtrTy(LLVMContext& ctx) {
    Type* int8_ty = IntegerType::get(ctx, 8);
    Type* int8_ptr_ty = int8_ty->getPointerTo(0);
    return int8_ptr_ty;
  }

  void instrumentFunctionEntry(Function& F) {
    if (F.isDeclaration() || F.isIntrinsic()) {
      return;
    }
    string gv_name = "xps." + F.getName().str() + ".in";
    outs() << gv_name << '\n';
    Value* str = _m->getGlobalVariable(gv_name);
    assert(str);

    Type* int8_ptr_ty = getInt8PtrTy(F.getContext());
    Instruction* FI = getFirstInst(F);
    auto casted_value = new BitCastInst(str, int8_ptr_ty, "", FI);

    std::vector<Value*> args;
    args.push_back(casted_value);
    Function* callee = _m->getFunction("printf");
    CallInst *new_inst = CallInst::Create(callee, args, "", FI);
  }

  bool runOnModule(Module& M) override {
    _m = &M;
    for (auto& F: M) {
      addFunctionStrDecl(F);
      instrumentFunctionEntry(F);
    }
    return true;
  }
};

char DynamicCallGraphPass::ID = 0;

static RegisterPass<DynamicCallGraphPass>
    DynamicCallGraphPassInfo("dyn-cg", "");

}
