//
// Created by tzhou on 3/5/18.
//

#ifndef LLVM_SCC_HPP
#define LLVM_SCC_HPP

#include <algorithm>
#include <string>
#include <fstream>
#include <cassert>
#include <Utils/PassCommons.h>
#include <llvm/Support/raw_ostream.h>

typedef std::string string;
//#define DEBUG_MODE

#ifdef DEBUG_MODE
#define DEBUG(s) s
#else
#define DEBUG(s) // s
#endif

using namespace llvm;

namespace xps {
class SCCHelper {
public:
  Module *_m;
  int _time;
  int _scc_id;
  std::map<Value*, int> _starts;
  std::map<Value*, int> _finishes;
  std::map<Value*, int> _lowlinks;
  std::vector<Function*> _stack;
  std::map<Function*, bool> _on_stack;
  std::map<Function*, int> _scc_map;
  std::set<int> _simple_sccs;
  std::map<int, std::set<Function*>> _scc_groups;
public:
  SCCHelper(Module* m) {
    _time = 0;
    _scc_id = 0;
    _m = m;
  }

  bool isVisited(Value* V) {
    if (_starts.find(V) == _starts.end()) {
      V->dump();
    }
    assert(_starts.find(V) != _starts.end());
    return _starts[V] != -1;
  }

  bool isFinished(Value* V) {
    assert(_finishes.find(V) != _finishes.end());
    return _finishes[V] != -1;
  }

  void markSCC() {
    for (auto& F: *_m) {
      _starts[&F] = -1;
      _finishes[&F] = -1;
    }

    for (auto& F: *_m) {
      if (!has_definition(&F)) {
        continue;
      }

      if (!isVisited(&F)) {
        markSCCImpl(&F);
      }
    }

    verifyTopoSort();
  }

  void setSCCID(Function* F, int id) {
    _scc_map[F] = id;
    if (_scc_groups.find(id) == _scc_groups.end()) {
      _scc_groups[id] = std::set<Function*>();
    }
    _scc_groups[id].insert(F);
  }

  int getSCCID(Function* F) {
    if (_scc_map.find(F) == _scc_map.end()) {
      outs() << "Function " << F->getName() << " level not found\n";
    }
    return _scc_map.at(F);
  }

  void updateLowLink(Function* F, int v) {
    if (_lowlinks[F] > v) {
      _lowlinks[F] = v;
    }
  }

  void markSCCImpl(Function* F) {
    DEBUG(outs() << "now: " << F->getName() << "\n");

    _starts[F] = _time++;
    _lowlinks[F] = _starts[F];
    _stack.push_back(F);
    _on_stack[F] = true;

    for (auto& B: *F) {
      for (auto& I: B) {
        if (CallSite CS = CallSite(&I)) {
          auto callee = get_callee(CS);
          if (!callee) {
            continue;
          }

          if (!has_definition(callee)) {
            continue;
          }

          if (!isVisited(callee)) {
            markSCCImpl(callee);
            updateLowLink(F, _lowlinks[callee]);
          }
          else if (_on_stack[callee]) {
            // Successor w is in stack S and hence in the current SCC
            // If w is not on stack, then (v, w) is a cross-edge in the
            // DFS tree and must be ignored
            // Note: The next line may look odd - but is correct.
            // It says w.index not w.lowlink; that is deliberate and from
            // the original paper
            //printCall(CS);
            updateLowLink(F, _starts[callee]);
          }
        }
      }
    }
    
    DEBUG(outs() << F->getName() << " "
           << _lowlinks[F] << " "
           << _starts[F] << "\n";)

    // If v is a root node, pop the stack and generate an SCC
    if (_lowlinks[F] == _starts[F]) {
      while (_stack[_stack.size()-1] != F) {
        auto top = _stack[_stack.size()-1];
        _stack.pop_back();
        _on_stack[top] = false;
        setSCCID(top, _scc_id);
      }
      assert(_stack[_stack.size()-1] == F);
      _stack.pop_back();
      _on_stack[F] = false;
      setSCCID(F, _scc_id);
      _scc_id++;
    }

    _finishes[F] = _time++;
  }

  void markSimpleSCCs() {
    int counter = 0;
    assert(_scc_groups.size() == _scc_id);
    for (auto it: _scc_groups) {
      bool is_simple = true;
      //outs() << it.first << "\n";
      for (auto F: it.second) {
        //outs() << F->getName() << "\n";
        if (!hasSingleInternalCaller(F)) {
          is_simple = false;
        }
        //outs() << F->getName() << "\n";
      }
      //outs() << it.first << "\n";

      if (is_simple) {
        _simple_sccs.insert(it.first);
        counter++;
      }
    }

    outs() << "simple scc: " << counter << "\n";
  }

  bool isSimpleSCC(int id) {
    return _simple_sccs.find(id) != _simple_sccs.end();
  }

  bool isOptimizableSCC(int scc) {
    if (isSimpleSCC(scc) && _scc_groups[scc].size() == 1) {
      return true;
    }
    else {
      return false;
    }
  }

  void getSuccEdges(int id, std::vector<Instruction*>& edges) {
    // Check call sites of every function in the scc group
    auto& group = _scc_groups.at(id);
    for (auto& F: group) {
      for (auto &B: *F) {
        for (auto &I: B) {
          if (CallSite CS = CallSite(&I)) {
            auto callee = get_callee(CS);
            if (!has_definition(callee)) {
              continue;
            }

            if (!isInternalEdge(CS)) {
              edges.push_back(&I);
            }
          }
        }
      }
    }
  }

  void getPredEdges(int id, std::vector<Instruction*>& edges) {
    // Check call sites of every function in the scc group
    auto& group = _scc_groups.at(id);
    for (auto& F: group) {
      for (auto I: get_callers(F)) {
        if (!isInternalEdge(CallSite(I))) {
          edges.push_back(I);
        }
      }
    }
  }

  void printSCCGroup(int id) {
    outs() << id << ": ";
    for (auto F: _scc_groups[id]) {
      outs() << F->getName() << ", ";
    }
    outs() << "\n";
  }

  bool hasSingleSuccessor(int id) {
    std::vector<Instruction*> edges;
    getSuccEdges(id, edges);
    return edges.size() == 1;
  }

  bool hasSinglePredecessor(int id) {
    std::vector<Instruction*> edges;
    getPredEdges(id, edges);
    return edges.size() == 1;
  }

  bool isInternalEdge(CallSite CS) {
    auto I = CS.getInstruction();
    auto caller = I->getFunction();
    auto callee = get_callee(CS);
    return getSCCID(caller) == getSCCID(callee);
  }

  bool hasSingleInternalCaller(Function* F) {
    int counter = 0;
    std::set<Instruction*> callers;
    get_callers(F, callers);
    for (auto I: callers) {
      if (isInternalEdge(CallSite(I))) {
        counter++;
      }
    }
    return counter == 1;
  }

  void verifyTopoSort() {
    std::map<int, bool> visited;
    for (int id = 0; id < _scc_id; ++id) {
      //outs() << "in " << id << "\n";
      std::vector<Instruction*> edges;
      getSuccEdges(id, edges);
      for (auto I: edges) {
        auto callee = get_callee(CallSite(I));
        int callee_scc = getSCCID(callee);
//        outs() << "succ: " << callee_scc << " "
//               << visited[callee_scc] << "\n"
//            ;
        assert(visited[callee_scc] == true);
      }
      visited[id] = true;
      //outs() << "out " << id << "\n";
    }

  }

  void printSCCID() {
    for (auto it: _scc_map) {
      Function* f = it.first;
      assert(f);

      if (!has_definition(f)) {
        outs() << f->getName() << " no definition\n";
      }
      assert(has_definition(f));
      outs() << f->getName() << " "
             << it.second << "\n"
          ;
    }
  }

  void printStats() {
    int counts[2] = {0};
    for (auto&F: *_m) {
      for (auto& B: F) {
        for (auto &I: B) {
          if (CallSite CS = CallSite(&I)) {
            auto caller = I.getFunction();
            auto callee = get_callee(CS);
            if (!callee) {
              continue;
            }

            if (!has_definition(&F)) {
              continue;
            }

            counts[0]++;
            if (_scc_map[caller] == _scc_map[callee]) {
              counts[1]++;
            }
          }
        }
      }
    }

    outs() << "[scc]:\n  ";
    outs() << "all edges:" << counts[0] << "\n  "
           << "internal edges:" << counts[1] << "\n";
  }
};
}

#endif //LLVM_SCC_HPP
