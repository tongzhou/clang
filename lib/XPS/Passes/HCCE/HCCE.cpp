//
// Created by tzhou on 12/23/17.
//

#ifndef XPS_HCCE_HPP
#define XPS_CtxRecord_H

#include <cmath>
#include <set>
#include <fstream>
#include <fcntl.h>
#include <llvm/Support/raw_ostream.h>
#include <llvm/Support/FileSystem.h>
#include <llvm/IR/LegacyPassManager.h>
#include <clang/XPS/Flags.h>
#include <Utils/XPSTypeCache.h>
#include <Utils/PassCommons.h>
#include <Passes/PCCE/BackEdgeHelper.hpp>
#include "SCC.hpp"

using namespace llvm;

namespace xps {

typedef std::string string;

static cl::opt<int> CtxSize("ctx-size", cl::init(128));
static cl::opt<string> QCaller("caller");
static cl::opt<string> QCallee("callee");
static cl::opt<bool> UseRLE("use-rle", cl::init(false));
static cl::opt<bool> DoBE("back-edge", cl::init(true));
static cl::opt<bool> OptCycle("opt-cycle", cl::init(true));
static cl::opt<bool> Baseline("baseline");
static cl::opt<bool> PrintPaths("print-paths");
static cl::opt<bool> PrintStats("print-stats");
static cl::opt<bool> DynCallLogger("dyn-logger");
  static cl::opt<bool> CheckMode("check-mode");

// Option: nlevel now means the layers of contexts
// nlevel=0: no context;
// nlevel=1: 1 layer of context; etc.

class HCCEPass: public ModulePass, public XPSTypeCache {
public:
  int _time = 0;
  int _max_level = 0;
  bool _verbose = true;
  bool _level_verbose = false;
  bool _instr_verbose = false;
  bool _use_stride = true;
  bool _run_length= false;
  bool _record_cv = false;
  bool _use_ics = false;
  bool _opt_singular_calls = true;

  Module* _m = nullptr;
  Type* _ctx_ty = nullptr;
  SCCHelper* _scc = nullptr;
  std::vector<size_t> _ids;
  std::vector<size_t> _counts;
  std::map<int, int> _scc_levels;
  std::set<Instruction*> _back_edges;
  std::ofstream _log;

public:
  static char ID;

  HCCEPass(): ModulePass(ID) {
    _counts.assign(10, 0);

    if (Baseline) {
      _run_length = false;
      _opt_singular_calls = false;
      _use_stride = false;
    }

    if (UseRLE) {
      _run_length = true;
    }
  }

  ~HCCEPass() {

  }

  bool isSingularCall(CallSite CS) {
    Function* f = CS.getCalledFunction();
    if (!f) {
      return false;
    }

    int callers = 0;
    for (auto I: get_callers(f)) {
      callers++;
    }

    return callers == 1;
  }

  bool hasSingleCaller(Function* F) {
    int callers = 0;
    for (auto I: get_callers(F)) {
      callers++;
    }

//    std::set<Instruction*> ic_callers;
//    _ic_helper->getICCallers(F, ic_callers);
//    callers += ic_callers.size();

    return callers == 1;
  }

  bool isBackEdge(Instruction* I) {
    return _back_edges.find(I) != _back_edges.end();
  }

  bool hasSingleCallee(Function* F) {
    int callsites = 0;
    for (auto& B: *F) {
      for (auto& I: B) {
        if (CallSite CS = CallSite(&I)) {
          callsites++;
        }
      }
    }
    return callsites == 1;
  }

  void setFunctionLevel(int scc, int L) {
    _scc_levels[scc] = L;
    if (L > _max_level) {
      _max_level = L;
    }
  }

  void updateSCCLevel() {

  }

  void calcSCCLevels() {
    for (int id = _scc->_scc_id-1; id >= 0; --id) {
      std::vector<Instruction*> edges;
      _scc->getPredEdges(id, edges);
      if (edges.empty()) {
        continue;
      }

      int highest = -1;
      for (auto I: edges) {
        Function* caller = I->getFunction();
        int caller_scc  = _scc->getSCCID(caller);
        if (_scc_levels[caller_scc] > highest) {
          highest = _scc_levels[caller_scc];
        }
      }

      if (edges.size() == 1 && _opt_singular_calls) {
        _scc_levels[id] = highest;
      }
      else {
        if (edges.size() < 256) {
          _scc_levels[id] = highest + 1;
        }
        else if (edges.size() < 65536) {
          _scc_levels[id] = highest + 2;
        }
        else {
          assert(0);
        }
      }

      if (_scc_levels[id] > _max_level) {
        _max_level = _scc_levels[id];
      }
    }
  }

  void printPOILevel() {
    string funcs[4] = {"malloc", "calloc", "realloc", "free"};
    for (auto& name: funcs) {
      Function* F = _m->getFunction(name);
      if (!F) {
        continue;
      }

      outs() << "[POI]\n  ";
      outs() << "name " << name << "\n  "
             << "level " << _scc_levels[_scc->getSCCID(F)] << "\n"
          ;
    }
  }

  void instrumentCallSites() {
    _ids.assign(CtxSize, 1);

    for (int id = _scc->_scc_id-1; id >= 0; --id) {
      std::vector<Instruction *> edges;
      _scc->getPredEdges(id, edges);
      int count = 1;
      for (auto I: edges) {
        doCallSite(CallSite(I), count++);
      }

      if (OptCycle) {
        if (_scc->isOptimizableSCC(id)) {
          _counts[2]++;
          doSCC(id);
        }
      }
    }
  }

  void doSCC(int scc) {
    for (auto F: _scc->_scc_groups[scc]) {
      int counters = 0;
      for (auto I: get_callers(F)) {
        if (I->getFunction() == F) {
          counters++;
          doBackEdge(CallSite(I), scc);
        }
      }

      if (counters != 1) {
        outs() << F->getName() << "\n";
      }
      assert(counters == 1);
      return;
    }
  }

  void doBackEdge(CallSite CS, int scc) {
    auto I = CS.getInstruction();
    int level = _scc_levels[scc];
    auto gep = insertGEP(I, level, "xps_cycles");
    auto bc = new BitCastInst(gep, Int32PtrTy, "", I);
    auto load = new LoadInst(bc, "", I);
    auto c = ConstantInt::get(Int32Ty, 1);
    auto add = BinaryOperator::Create(Instruction::Add,
                                      load, c, "", I);
    auto store = new StoreInst(add, bc, I);

    //auto s = I->getNextNode();
    for (auto s: get_succ_insts(I)) {
      gep = insertGEP(I, level, "xps_cycles");
      bc = new BitCastInst(gep, Int32PtrTy, "", I);
      load = new LoadInst(bc, "", s);
      add = BinaryOperator::Create(Instruction::Sub,
                                   load, c, "", s);
      store = new StoreInst(add, bc, s);
    }
  }
//
//  void queryCallSite(Instruction* I,
//                     Function* callee) {
//    Function* caller = I->getFunction();
//    int caller_level = _scc_levels.at(caller);
//    int callee_level = _scc_levels.at(callee);
//    outs() << caller->getName() << " "
//           << caller_level << " -> ";
//    outs() << callee->getName() << " "
//           << callee_level;
//    outs() << "\n";
//
//    DILocation* loc = I->getDebugLoc();
//    if (loc) {
//      outs() << "loc: "
//             << loc->getFilename() << ":"
//             << loc->getLine() << "\n";
//    }
//
//    int i = caller_level;
//    for (; i < callee_level; ++i) {
//      outs() << i << " ";
//      outs().write_hex(_ids[i]);
//      outs() << "\n";
//    }
//  }

  int getCallerLevel(CallSite CS) {
    auto I = CS.getInstruction();
    Function* caller = I->getFunction();
    return _scc_levels[_scc->getSCCID(caller)];
  }

  int getCalleeLevel(CallSite CS) {
    auto callee = get_callee(CS);
    return _scc_levels[_scc->getSCCID(callee)];
  }

  void doCallSite(CallSite CS, int s) {
    auto callee = get_callee(CS);
    if (!has_definition(callee)) {
      return;
    }

    auto I = CS.getInstruction();
    assert(!isBackEdge(I));

    Function* caller = I->getFunction();
//    if (caller->getName().equals(QCaller) &&
//        callee->getName().equals(QCallee)) {
//      queryCallSite(I, callee);
//    }

    int caller_level = getCallerLevel(CS);
    int callee_level = getCalleeLevel(CS);
    if (_instr_verbose) {
      outs() << "caller lv: " << caller_level << " "
             << "callee lv: " << callee_level << "\n"
          ;
    }

    if (caller_level > callee_level) {
      errs() << "bad scc: " << _scc->getSCCID(caller) << " -> "
             << _scc->getSCCID(callee) << "\n"
          ;
      errs() << "caller lv: " << caller_level << " "
             << "callee lv: " << callee_level << "\n"
          ;
      I->dump();
    }
    assert(caller_level <= callee_level);
    if (caller_level == callee_level) {
      return;
    }

    int distance = callee_level - caller_level;
//    if (distance > 4) {
//      outs() << "dist: " << distance << "\n"
//             << "callee: " << callee_level << "\n"
//             << "caller: " << caller_level << "\n"
//             << caller->getName() << "\n"
//          ;
//    }

    if (_run_length && distance > 8) {
      doRunLengthEncoding(CS);
    }
    else {
      int i = caller_level;
      /* Store by 16 bits for the rest */
      for (; i < callee_level; i+=8) {
        instrumentEightLevels(I, i, s);
      }
    }

    /* number of instrumented sites */
    _counts[0]++;
  }

  // @deprecated
  // These two functions need to be rewritten because they
  // still use the old _ids stuff
  void doRunLengthEncoding(CallSite CS) {
    auto I = CS.getInstruction();
    int l1 = getCallerLevel(CS);
    int l2 = getCalleeLevel(CS);
    setControlCtx(I, l1, l2);
  }

  void setControlCtx(Instruction* I, int l1, int l2) {
    auto gep = insertGEP(I, l1);
    auto bc = new BitCastInst(gep, Int64Ty->getPointerTo(0),
                              "", I);
    short values[4];
    // [Layout]
    // slot l1+0: _ids[l1]
    // slot l1+1: 0 (control char)
    // slot l1+2: l2 - l1
    values[0] = _ids[l1];
    values[1] = 0;
    values[2] = l2 - l1;
    values[3] = 0;
    auto c = ConstantInt::get(Int64Ty,
                              *((size_t *)values));
    auto store = new StoreInst(c, bc, I);

    _ids[l1]++;
    _counts[1]++;
  }

  GetElementPtrInst* insertGEP(Instruction* I, int level,
                               string gv="xps_ctx") {
    auto ctx_var = _m->getGlobalVariable(gv);
    Value* idx[2];
    idx[0] = ConstantInt::get(Int32Ty, 0);
    idx[1] = ConstantInt::get(Int32Ty, level);
    auto gep = GetElementPtrInst::Create(nullptr, ctx_var,
                                         idx, "", I);
    return gep;
  }

  string getNameFromLevel(int level, int stride) {
    string s = "ctx";
    for (int i = level; i < level+stride; ++i) {
      s += "_" + std::to_string(i);
      char hex[10];
      sprintf(hex, "%04x", (short)_ids[i]);
      s += "_";
      s += hex;
    }
    return s;
  }

  void instrumentFourLevels(Instruction* I, int level) {
    auto gep = insertGEP(I, level);
    auto bc = new BitCastInst(gep, Int64PtrTy, "", I);
    size_t level_id = getFourLevelID(level);
    auto c = ConstantInt::get(Int64Ty, level_id);
    auto store = new StoreInst(c, bc, I);
    bc->setName(getNameFromLevel(level, 4));

    for (int i = 0; i < 4; ++i) {
      _ids[level+i]++;
    }
    _counts[1]++;
  }

  void instrumentEightLevels(Instruction* I, int level, int s) {
    auto gep = insertGEP(I, level);
    auto bc = new BitCastInst(gep, Int64Ty->getPointerTo(0), "", I);
    auto c = ConstantInt::get(Int64Ty, s);
    auto store = new StoreInst(c, bc, I);

    _counts[1]++;
  }

  size_t getFourLevelID(int l) {
    size_t id = _ids[l]
        + (_ids[l+1] << 16)
        + (_ids[l+2] << 32)
        + (_ids[l+3] << 48)
    ;
    return id;
  }

  size_t getCompoundID(int l, int stride) {
    size_t id = 0;
    for (int i = 0; i < stride; ++i) {
      id += (_ids[l+i] << 16*i);
    }
    return id;
  }

  GlobalVariable* addGlobalStr(Twine name, Twine value) {
    Constant *c = ConstantDataArray::getString(_m->getContext(),
                                               value.str());
    auto *gv = new GlobalVariable(*_m, c->getType(), true,
                                  GlobalValue::ExternalLinkage,
                                  c, name);
    return gv;
  }

  void insertLogCall(Instruction* I, int level) {
    Function* F = I->getFunction();
    auto gv_name = "xps." + F->getName().str();
    auto gv = _m->getGlobalVariable(gv_name);
    if (!gv) {
      gv = addGlobalStr(gv_name, F->getName());
    }

    auto bc = new BitCastInst(gv, Int8PtrTy, "", I);
    Value* v1 = ConstantInt::get(Int32Ty, level);
    Value* v2 = ConstantInt::get(Int16Ty, _ids[level]);
    std::vector<Value*> args_vec {bc, v1, v2};
    Function* callee = _m->getFunction("ctx_log");
    CallInst::Create(callee, args_vec, "", I);
  }

  void insertBackTrace(Instruction* I) {
    std::vector<Value*> args_vec
        { ConstantInt::get(Int32Ty, 12345)};
    Function* callee = _m->getFunction("printBacktrace");
    CallInst::Create(callee, args_vec, "", I);
  }

  void instrumentOneLevel(Instruction* I, int level) {
    insertGEPStore(I, level);
    if (DynCallLogger) {
      insertLogCall(I, level);
    }

    _ids[level]++;
    _counts[1]++;
  }

  void insertGEPStore(Instruction* I, int level) {
    auto gep = insertGEP(I, level);
    auto c = ConstantInt::get(Int16Ty, _ids[level]);
    auto store = new StoreInst(c, gep, I);
    gep->setName(getNameFromLevel(level, 1));
  }

  size_t getLevelValue(size_t value, int level) {
    int shift = level * 16;
    return (value << shift);
  }

  void appendNewGlobals() {
    ArrayType *ty = ArrayType::get(_ctx_ty, CtxSize);


    // Add one more param GlobalVariable::GeneralDynamicTLSModel
    // to make it thread-local
    new GlobalVariable(*_m, ty, false,
                                 GlobalValue::ExternalLinkage,
                                 nullptr, "xps_ctx", nullptr);
    new GlobalVariable(*_m, ty, false,
                       GlobalValue::ExternalLinkage,
                       nullptr, "xps_cycles", nullptr);
    new GlobalVariable(*_m, Int32Ty, false,
                       GlobalValue::ExternalLinkage,
                       nullptr, "xps_ctx_level", nullptr);

    insertFuncDecl();
  }

  void insertFuncDecl() {
    std::vector<Type*> arg_types{ Int64Ty, Int32Ty };
    FunctionType *FT = FunctionType::get(VoidTy, arg_types, false);
    Function::Create(FT, Function::ExternalLinkage, "hcce_malloc", _m);
    
  }

  void insertInitCode() {
    Function* main = _m->getFunction("main");
    assert(main);
    auto I = main->front().getFirstNonPHI();
    insertCtxLevelInitCode(I);
  }

  void insertCtxLevelInitCode(Instruction* I) {
    auto gv = _m->getGlobalVariable("xps_ctx_level");

    while (_max_level % 8 != 0) {
      _max_level++;
    }

        auto c = ConstantInt::get(Int64Ty, _max_level);
            auto store = new StoreInst(c, gv, I);
  }
//  void printQueryFuncStats() {
//    for (auto name: _query_funcs) {
//      Function* f = _m->getFunction(name);
//      if (!f) {
//        continue;
//      }
//      outs() << "[" << name << "]\n"
//             << "  level: " << _scc_levels[f] << "\n"
//             << "  count: " << _freqs[f] << '\n'
//          ;
//    }
//  }

  void printIDStats() {
    outs() << "[ids]\n";
    for (int i = 0; i < _ids.size(); ++i) {
      outs() << "  " << i << ": " << _ids[i] << '\n';
    }
  }

  void printBackEdgeStats() {
    size_t num = _back_edges.size();
    outs() << "[back edge]\n";
    outs() << "  " << num << '\n';
  }

  void printStats() {
    errs() << "max: " << _max_level << "\n";
    errs() << "size: " << CtxSize << "\n";
    errs() << "sites: " << _counts[0] << "\n";
    errs() << "simple sccs: " << _counts[2] << "\n";
//    size_t id_count = 0;
//    for (auto i: _ids) {
//      id_count += i;
//    }
//    errs() << "instrumented: " << id_count << "\n";
    errs() << "actual stores: " << _counts[1] << "\n";

    printBackEdgeStats();
    //printStatsToFile();


//    std::vector<size_t> nums(_max_level+1, 0);
//    for (auto it: _scc_levels) {
//      Function* f = it.first;
//      int l = it.second;
//      nums[l]++;
//    }

//    outs() << "========= Stats =========\n";
//    for (int i = 0; i < nums.size(); ++i) {
//      outs() << "level " << i
//             << " " << nums[i]
//             << "\n";
//    }
//    outs() << "=========================\n";
  }

  void printStatsToFile() {
    FILE* f = fopen("cr-funcs.txt", "w");
    raw_fd_ostream OS(fileno(f), true);
    for (auto it: _scc_levels) {
      auto scc = it.first;
      int l = it.second;
      if (l == -1) {
        continue;
      }
      OS << scc << " " << l << "\n";
    }
    OS.close();
  }

  bool runOnModule(Module& M) override {
    XPSTypeCache::initialize(M.getContext());
    _m = &M;
    _ctx_ty = Int16Ty;
    _scc = new SCCHelper(_m);
    _scc->markSCC();
    _scc->markSimpleSCCs();
    //_scc->printSCCID();
    _scc->printStats();


    BackEdgeHelper beh;
    beh.markBackEdge(_back_edges, M);
    
    calcSCCLevels();
    printPOILevel();
    appendNewGlobals();
    instrumentCallSites();

    if (PrintStats) {
      printStats();
    }

    return true;
  }
};

char HCCEPass::ID = 0;


static RegisterPass<HCCEPass> HCCEPassInfo("hcce", "Array-based Calling Context Encoding Pass",
                                               false /* Only looks at CFG */,
                                               false /* Analysis Pass */);

}

#endif
