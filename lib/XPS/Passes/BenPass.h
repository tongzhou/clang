//
// Created by tzhou on 12/23/17.
//

#ifndef LLVM_Ben_H
#define LLVM_Ben_H

#include <iostream>
#include <llvm/Transforms/Utils/BasicBlockUtils.h>
#include <llvm/IR/Constants.h>
#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Type.h"
#include "llvm/IR/Instruction.h"
#include "llvm/IR/Instructions.h"
#include "../Utils/XPSTypeCache.h"

using namespace llvm;

namespace xps {

typedef std::string string;

struct MFunc {
  string old_name;
  string new_name;
  bool add_id;

  MFunc(string oldname, string newname, bool addid):
      old_name(oldname), new_name(newname), add_id(addid) {}
};

class BenPass: XPSTypeCache {
public:
  string _lang;
  std::vector<MFunc*> _alloc_set;
  std::vector<MFunc*> _free_set;

public:
  BenPass() {
    _lang = "all";
    init_lang();
  }

  void init_lang() {
    if (_lang == "c" || _lang == "cpp" || _lang == "all") {
      _alloc_set.push_back(new MFunc("malloc", "ben_malloc", true));
      _alloc_set.push_back(new MFunc("calloc", "ben_calloc", true));
      _alloc_set.push_back(new MFunc("realloc", "ben_realloc", true));
      _free_set.push_back(new MFunc("free", "ben_free", false));
    }



    if (_lang == "cpp" || _lang == "all") {
      _alloc_set.push_back(new MFunc("_Znam", "ben_malloc", true));
      _alloc_set.push_back(new MFunc("_Znwm", "ben_malloc", true));
      _free_set.push_back(new MFunc("_ZdaPv", "ben_free", false));
      _free_set.push_back(new MFunc("_ZdlPv", "ben_free", false));
    }
//
//    if (_lang == "flang" || _lang == "all") {
//      _alloc_set.push_back(new MFunc("f90_alloc", "f90_ben_alloc", true));
//      _alloc_set.push_back(new MFunc("f90_alloc03", "f90_ben_alloc03", true));
//      _alloc_set.push_back(new MFunc("f90_alloc03_chk", "f90_ben_alloc03_chk", true));
//      _alloc_set.push_back(new MFunc("f90_alloc04", "f90_ben_alloc04", true));
//      _alloc_set.push_back(new MFunc("f90_alloc04_chk", "f90_ben_alloc04_chk", true));
//
//      _alloc_set.push_back(new MFunc("f90_kalloc", "f90_ben_kalloc", true));
//      _alloc_set.push_back(new MFunc("f90_calloc", "f90_ben_calloc", true));
//      _alloc_set.push_back(new MFunc("f90_calloc03", "f90_ben_calloc03", true));
//      _alloc_set.push_back(new MFunc("f90_calloc04", "f90_ben_calloc04", true));
//      _alloc_set.push_back(new MFunc("f90_kcalloc", "f90_ben_kcalloc", true));
//      _alloc_set.push_back(new MFunc("f90_ptr_alloc", "f90_ben_ptr_alloc", true));
//      _alloc_set.push_back(new MFunc("f90_ptr_alloc03", "f90_ben_ptr_alloc03", true));
//      _alloc_set.push_back(new MFunc("f90_ptr_alloc04", "f90_ben_ptr_alloc04", true));
//      _alloc_set.push_back(new MFunc("f90_ptr_src_alloc03", "f90_ben_ptr_src_alloc03", true));
//      _alloc_set.push_back(new MFunc("f90_ptr_src_alloc04", "f90_ben_ptr_src_alloc04", true));
//      _alloc_set.push_back(new MFunc("f90_ptr_src_calloc03", "f90_ben_ptr_src_calloc03", true));
//      _alloc_set.push_back(new MFunc("f90_ptr_src_calloc04", "f90_ben_ptr_src_calloc04", true));
//      _alloc_set.push_back(new MFunc("f90_ptr_kalloc", "f90_ben_ptr_kalloc", true));
//      _alloc_set.push_back(new MFunc("f90_ptr_calloc", "f90_ben_ptr_calloc", true));
//      _alloc_set.push_back(new MFunc("f90_ptr_calloc03", "f90_ben_ptr_calloc03", true));
//      _alloc_set.push_back(new MFunc("f90_ptr_calloc04", "f90_ben_ptr_calloc04", true));
//      _alloc_set.push_back(new MFunc("f90_ptr_kcalloc", "f90_ben_ptr_kcalloc", true));
//      _alloc_set.push_back(new MFunc("f90_auto_allocv", "f90_ben_auto_allocv", true));
//      _alloc_set.push_back(new MFunc("f90_auto_alloc", "f90_ben_auto_alloc", true));
//      _alloc_set.push_back(new MFunc("f90_auto_alloc04", "f90_ben_auto_alloc04", true));
//      _alloc_set.push_back(new MFunc("f90_auto_calloc", "f90_ben_auto_calloc", true));
//      _alloc_set.push_back(new MFunc("f90_auto_calloc04", "f90_ben_auto_calloc04", true));
//
//      _free_set.push_back(new MFunc("f90_dealloc", "f90_ben_dealloc", false));
//      _free_set.push_back(new MFunc("f90_dealloc03", "f90_ben_dealloc03", false));
//      _free_set.push_back(new MFunc("f90_dealloc_mbr", "f90_ben_dealloc_mbr", false));
//      _free_set.push_back(new MFunc("f90_dealloc_mbr03", "f90_ben_dealloc_mbr03", false));
//      _free_set.push_back(new MFunc("f90_deallocx", "f90_ben_deallocx", false));
//      _free_set.push_back(new MFunc("f90_auto_dealloc", "f90_ben_auto_dealloc", false));
//      // todo
//    }
  }

  void insert_ben_declarations(Module* module) {
    insert_ben_malloc(module);
    insert_ben_calloc(module);
    insert_ben_realloc(module);
    //insert_ben_free(module);  // do_free will automatically create ben_free declaration
  }

  void insert_ben_malloc(Module* module) {
    std::vector<Type*> arg_types{ Int64Ty };

    FunctionType *FT = FunctionType::get(Int8PtrTy, arg_types, false);
    Function *F = Function::Create(FT, Function::ExternalLinkage, "ben_malloc", module);
    F->addAttribute(llvm::AttributeSet::ReturnIndex, llvm::Attribute::NoAlias);
  }

  void insert_ben_calloc(Module* module) {
    std::vector<Type*> arg_types{ Int64Ty, Int64Ty };

    FunctionType *FT = FunctionType::get(Int8PtrTy, arg_types, false);
    Function *F = Function::Create(FT, Function::ExternalLinkage, "ben_calloc", module);
    F->addAttribute(llvm::AttributeSet::ReturnIndex, llvm::Attribute::NoAlias);
  }

  void insert_ben_realloc(Module* module) {
    llvm::LLVMContext& ctx = module->getContext();
    std::vector<Type*> arg_types{ Int8PtrTy, Int64Ty };

    FunctionType *FT = FunctionType::get(Int8PtrTy, arg_types, false);
    Function *F = Function::Create(FT, Function::ExternalLinkage, "ben_realloc", module);
  }

  void insert_ben_free(Module* module) {
    llvm::LLVMContext& ctx = module->getContext();
    std::vector<Type*> arg_types{ Int8PtrTy };

    FunctionType *FT = FunctionType::get(VoidTy, arg_types, false);
    Function *F = Function::Create(FT, Function::ExternalLinkage, "ben_free", module);
  }

  void do_frees(Module* module) {
    for (auto tf: _free_set) {
      if (llvm::Function* f = module->getFunction(tf->old_name)) {
        f->setName(tf->new_name);
        std::cout << "new name: " << f->getName().str() << '\n';
      }
    }
  }

  void do_allocs(Module* module) {
    for (auto mf: _alloc_set) {
      //printf("do %s\n", mf->old_name.c_str());
      if (llvm::Function* f = module->getFunction(mf->old_name)) {
        for (auto U: f->users()) {  // can't be auto&
          if (CallInst* ci = dyn_cast<CallInst>(U)) {
            //ci->setCalledFunction(module->getFunction("malloc1"));  // this hangs forever

            Value* id_arg = ConstantInt::get(Int32Ty, 99);
            std::vector<Value*> args_vec;
            for (int h = 0; h < ci->getNumArgOperands(); ++h) {
              args_vec.push_back(ci->getArgOperand(h));
            }
            args_vec.insert(args_vec.begin(), id_arg);

            Function* callee = module->getFunction(mf->new_name);
            CallInst *new_inst = CallInst::Create(callee, args_vec, "");
            new_inst->setAttributes(ci->getAttributes());
            new_inst->copyMetadata(*ci);
            if (ci->getDebugLoc()) {
              new_inst->setDebugLoc(ci->getDebugLoc());
            }
            /* ci does not have a name yet at this point */
            //ci->dump();
            //new_inst->dump();

            BasicBlock::iterator ii(ci);
            ReplaceInstWithInst(ci->getParent()->getInstList(), ii, new_inst);
            break;
//            for (auto& ARG: ci->arg_operands()) {  // has to be auto&
//              ARG->dump();
//            }
          }
        }
      }
    }    
  }

  void run_on_module(Module * module) {
    XPSTypeCache::initialize(module->getContext());
    insert_ben_declarations(module);

    do_allocs(module);
    do_frees(module);

    auto malloc_f = module->getFunction("malloc1");
    //printf("malloc: %p\n", malloc_f);

    //module->dump();
  }
};

}

#endif //LLVM_Ben_H
