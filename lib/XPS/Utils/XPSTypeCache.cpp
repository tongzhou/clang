#include "XPSTypeCache.h"

namespace xps {
void XPSTypeCache::initialize(llvm::LLVMContext& ctx) {
  // Initialize the type cache.
  VoidTy = llvm::Type::getVoidTy(ctx);
  Int8Ty = llvm::Type::getInt8Ty(ctx);
  Int16Ty = llvm::Type::getInt16Ty(ctx);
  Int32Ty = llvm::Type::getInt32Ty(ctx);
  Int64Ty = llvm::Type::getInt64Ty(ctx);
  FloatTy = llvm::Type::getFloatTy(ctx);
  DoubleTy = llvm::Type::getDoubleTy(ctx);

  Int8PtrTy = Int8Ty->getPointerTo(0);
  Int8PtrPtrTy = Int8PtrTy->getPointerTo(0);

  Int32PtrTy = Int32Ty->getPointerTo(0);
  assert(Int32PtrTy->getElementType() == Int32Ty);
  Int64PtrTy = Int64Ty->getPointerTo(0);

  assert(Int32PtrTy->getElementType() == Int32Ty);
  // Some other ways to create these types
  // llvm::IntegerType *i32_ty = llvm::IntegerType::get(ctx, 32);
  // llvm::PointerType *i8_ptr_ty = llvm::PointerType::getInt8PtrTy(ctx);
}
}
