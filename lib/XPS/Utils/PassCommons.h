//
// Created by tzhou on 12/29/17.
//

#ifndef LLVM_XPS_PASSCOMMONS_H
#define LLVM_XPS_PASSCOMMONS_H

#include "llvm/IR/InstrTypes.h"
#include "llvm/IR/Instruction.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/DebugInfo.h"
#include <llvm/IR/CallSite.h>
#include <llvm/IR/Value.h>
#include <llvm/IR/Module.h>
#include <llvm/IR/Constant.h>
#include <llvm/IR/Constants.h>
#include <llvm/Support/raw_ostream.h>
#include <set>

namespace xps {

llvm::Function* get_callee(llvm::CallSite CS);
void get_callers(llvm::Function* F, std::set<llvm::Instruction*>& insts);
std::set<llvm::Instruction*> get_callers(llvm::Function* F);
std::set<llvm::Instruction*> get_succ_insts(llvm::Instruction* I);
void print_call(llvm::CallSite CS);
bool has_definition(llvm::Function* F);
void get_first_level_callsites(llvm::Module& M, std::set<llvm::Instruction*>& sites);
void get_upper_level_callsites(std::set<llvm::Instruction*>& callees, std::set<llvm::Instruction*>& callers);

}

#endif //LLVM_XPS_PASSCOMMONS_H
