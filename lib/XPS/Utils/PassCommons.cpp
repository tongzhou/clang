//
// Created by tzhou on 12/29/17.
//

#include "PassCommons.h"
#include "../Passes/BenPass.h"

using namespace llvm;

namespace xps {

void printCall(CallSite CS) {
  auto I = CS.getInstruction();
  errs() << I->getFunction()->getName() << ":";
  I->dump();
}

Function* get_callee(CallSite CS) {
  auto I = CS.getInstruction();
  if (CS.isIndirectCall()) {
    return NULL;
  }

  Value* v = CS.getCalledValue();
  if (!v) {
    return NULL;
  }
  Function* f;

  if (auto CE = dyn_cast<ConstantExpr>(v)) {
    v = CE->getOperand(0);
  }

  f = dyn_cast<Function>(v);
  if (!f) {
    //I->dump();
    GlobalAlias* ga = dyn_cast<GlobalAlias>(v);
    assert(ga);
    f = dyn_cast<Function>(ga->getAliasee());
  }

  assert(f);
  return f;
}

void get_callers(Function* F, std::set<Instruction*>& insts) {
  for (auto User: F->users()) {
    if (CallSite CS = CallSite(User)) {
      if (get_callee(CS) == F) {
        insts.insert(CS.getInstruction());
      }
    }
  }
}

std::set<Instruction*> get_callers(Function* F) {
  std::set<Instruction*> callers;
  get_callers(F, callers);
  return callers;
}

bool has_definition(Function* F) {
  if (!F) {
    return false;
  }

  // This fails astar's _ZN15largesolidarrayIP6regobjE6createEi
//  if (F->hasExactDefinition()) {
//    return true;
//  }
  if (F->isIntrinsic()) {
    return false;
  }

  if (!F->isDeclaration()) {
    return true;
  }

  string name = F->getName().str();
  if (name == "malloc" || name == "calloc"
      || name == "realloc" || name == "free") {
    return true;
  }

  return false;
}

std::set<Instruction*> get_succ_insts(Instruction* I) {
  std::set<Instruction*> nodes;
  if (auto i = dyn_cast<InvokeInst>(I)) {
    assert(!I->getNextNode());
    nodes.insert(i->getNormalDest()->getFirstNonPHI());
    //nodes.insert(i->getLandingPadInst()->getNextNode());
  }
  else {
    assert(I->getNextNode());
    nodes.insert(I->getNextNode());
  }
  return nodes;
}

void get_first_level_callsites(Module &M, std::set<Instruction *> &sites) {
  auto ben = new BenPass();
  for (auto i: ben->_alloc_set) {
    if (auto F = M.getFunction(i->old_name)) {
      std::set<Instruction*> callers;
      get_callers(F, callers);
      sites.insert(callers.begin(), callers.end());
    }
  }
}

void get_upper_level_callsites(std::set<Instruction *> &callees,
                               std::set<Instruction *> &callers) {
  for (auto i: callees) {
    Function* callee = i->getFunction();
    std::set<Instruction*> set;
    get_callers(callee, set);
    for (auto caller: set) {
      if (callees.find(caller) != callees.end()) {
        continue;
      }
      callers.insert(caller);
    }
  }
}

}