//
// Created by tzhou on 12/23/17.
//

#include <iomanip>
#include <cassert>
#include <iostream>
#include "clang/XPS/Flags.h"

namespace xps {

GENERATE_RUNTIME_FLAGS(DEFINE_DEVELOP_FLAG)


#define ADD_FLAG_TO_MAP(type, name, value, doc)                ty = #type; _map[#name] = new Flag(ty[0], &name);
#define DELETE_FLAG(type, name, value, doc)                    delete _map[#name];

#define PRINT_FLAG(type, name, value, doc) \
  std::cout \
  << "flag: " \
  << std::left << std::setw(30) << #name \
  << " type: " \
  << std::left << std::setw(10) << #type \
  << " default: " \
  << std::left << std::setw(10) << value \
  << " doc: " \
  << doc << "\n";

std::map<std::string, Flag*> Flags::_map;
std::string Flags::env_prefix = "XPS_";


void Flags::init() {
  std::string ty;
  GENERATE_RUNTIME_FLAGS(ADD_FLAG_TO_MAP)
}

void Flags::destroy() {
  GENERATE_RUNTIME_FLAGS(DELETE_FLAG)
}

void Flags::print_flags() {
  GENERATE_RUNTIME_FLAGS(PRINT_FLAG)
}

Flag* Flags::get_flag(std::string key) {
  if (_map.find(key) == _map.end()) {
    return NULL;
  }
  else {
    return _map[key];
  }
}

void Flags::parse(llvm::StringRef key) {
  if (!key.startswith("-XX:")) {
    return;
  }

  size_t xx_len = strlen("-XX:");
  if (key[xx_len] == '+') {
    Flags::set_flag(key.substr(xx_len+1), true);
  }
  else if (key[xx_len] == '-') {
    Flags::set_flag(key.substr(xx_len+1), false);
  }
  else {
    llvm::StringRef opt = key.substr(xx_len);
    int pos = opt.find('=');
    assert(pos != llvm::StringRef::npos && "Bad formatted option, expect a `=`");
    Flags::set_flag(opt.substr(0, pos), opt.substr(pos+1));
    //guarantee(0, "Bad formatted option: %s (miss a '+' or '-'?)", _text.c_str());
  }
}



std::string Flags::get_env_name(std::string flag) {
  return "XPS_" + flag;
}

void Flags::set_flag_in_env(std::string opt) {
  if (opt[0] == '+') {
    set_xps_env(opt.substr(1), "1");
  }
  else if (opt[0] == '-') {
    set_xps_env(opt.substr(1), "0");
  }
  else {
    int pos = opt.find('=');
    assert(pos != llvm::StringRef::npos && "Bad formatted option, expect a `=`");
    set_xps_env(opt.substr(0, pos), opt.substr(pos+1));
  }
}

void Flags::set_xps_env(std::string flag, std::string value) {
  std::string name = "XPS_" + flag;
  setenv(name.c_str(), value.c_str(), 1);
}

char* Flags::get_xps_env(std::string flag) {
  std::string name = "XPS_" + flag;
  return getenv(name.c_str());
}



/**@brief Set boolean flags such as -XX:+MyFlag.
 *
 * @param key
 * @param v
 */
void Flags::set_flag(std::string key, bool v) {
  Flag* f = get_flag(key);
  if (f == NULL) {
    fprintf(stderr, "Unrecognized option: %s\n", key.c_str());
    std::terminate();
  }
  assert(f->type == 'b' && "option type must be bool");
  *(bool*)(f->value) = v;

  if (key == "UseSplitModule") {
    set_flag("ParallelModule", true);
  }
}

//void Flags::set_flag(std::string key, int v) {
//    Flag* f = get_flag(key);
//    if (f == NULL) {
//        fprintf(stderr, "Unrecognized option: %s\n", key.c_str());
//        std::terminate();
//    }
//    assert(f->type == 'i' && "option type must be int");
//    *(int*)(f->value) = v;
//}

/**@brief Set flags that have a value such as -XX:MyFlag=value
 *
 * @param key
 * @param value
 */
void Flags::set_flag(std::string key, std::string value) {
  Flag* f = get_flag(key);
  if (f == NULL) {
    fprintf(stderr, "Unrecognized option: %s\n", key.c_str());
    std::terminate();
  }
  char type = f->type;
  void* v = f->value;
  switch (type) {
  case 's':
    *(std::string*)v = value;
    break;
  case 'i':
    *(int*)v = std::stoi(value);
    break;
  default:
    fprintf(stderr, "Unsupported flag type: %c", type);
    std::terminate();
  }


//    assert(f->type == 's' && "option type must be std::string");
//    *(std::string*)(f->value) = value;


//    else {
//        std::string& type = f->type;
//        void* v;
//        if (type == "bool") {
//            *(bool*)v = std::stoi(value);
//        }
//        else if (type == "std::string") {
//            *(std::string*)v = value;
//        }
//        else if (type == "int") {
//            *(int*)v = std::stoi(value);
//        }
//        else {
//            fprintf(stderr, "Unsupported flag type: %s", type.c_str());
//            std::terminate();
//        }
//    }
}

bool Flags::get_bool_flag(const char* key) {
  char* env = get_xps_env(key);
  if (env) {
    //printf("flag %s: %s\n", key, env);
    return (bool)std::stoi(env);
  }
  else {
    return false;
  }
}

int Flags::get_int_flag(const char* key) {
  char* env = get_xps_env(key);
  if (env) {
    //printf("flag %s: %s\n", key, env);
    return std::stoi(env);
  }
  else {
    return 0;
  }
}

const char* Flags::get_cstr_flag(const char* key) {
  return get_xps_env(key);
}

bool Flags::has_flag(const char* key) {
  if (get_xps_env(key)) {
    return true;
  }
  else {
    return false;
  }
}

} // end namespace xps

