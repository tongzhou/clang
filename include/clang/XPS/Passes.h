//
// Created by tzhou on 12/26/17.
//

#ifndef LLVM_PASSES_H
#define LLVM_PASSES_H

#include <llvm/IR/LegacyPassManager.h>
#include <llvm/Transforms/Utils/BasicBlockUtils.h>
#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Instruction.h"
#include "llvm/Pass.h"
#include "clang/Frontend/CodeGenOptions.h"

namespace xps {

class Passes {
public:
  //static const clang::CodeGenOptions* _cg_opts;
  static void runModulePasses(llvm::Module* module);
  static int addXPSModulePasses(llvm::legacy::PassManager& PM);
  static llvm::ModulePass* getModulePass(std::string name);
};

}

#endif //LLVM_PASSES_H
